# Jenkins 應用與介紹
## 目錄
* [簡介](#簡介)
* [架設Jenkins](#架設Jenkins)
  * [安裝] (#架設Jenkins)
  * [設定流程] (#架設Jenkins)
  * [管理Jenkins](#管理)
  * [Cluster](#Cluster) - **加快運行速度**
* [CI日常](#測試流程)
  * [第一個IOS測試專案](#開啟新專案)
  		* [開始編譯](#開始編譯)
  		* [加入測試](#加入測試)
  		* [Miktake的CI,CD流程](#miktake的CI,CD流程)
  		* [測試報告產出與紀錄]
  		* [Unit Test] ()
  		* [covereage] ()
		* [Webhook] (#Webhook)

* [CD日常](#)
* [其他相關]()
	* [與gitlab比較] ()
* [疑難排除](#疑難排除)
* [參考] ()




<a name="簡介"></a>
## 前言
Jenkins是由Java撰寫的開源持續整合工具，可以幫助使用者達成專案建置、測試及部署等階段自動化的目標，是實現測試自動化及持續整合的利器


<a name="架設Jenkins"></a>
## 架設Jenkins

### 安裝
* 為了可以快速的安裝與管理,這裡使用homebrew來管理

1. 首先在安裝jenkins之前,jenkins是由java撰寫,所以先安裝java
	
	brew tap caskroom/versions   
	brew cask install java8
> 注意!!!!!!!!! 這裡要確保是使用java8,否則會有異常現象發生,所以可以透過以上指令來指定安裝版本

2. 接著安裝jenkins
	
		brew update && brew install jenkins
	
3.	啟動jenkins
	
		brew services start jenkins
	
	* 或是你也可以手動啟動

			jenkins

4.	安裝完後會有一組**預設密碼**,可透過以下指令來查看預設密碼

		more /Users/username/.jenkins/secrets/initialAdminPassword 
	
	> 如 : 07a27edb4d1c4b53b2598fde3adxxxXX

### 設定Jenkins


1.	先連上jenkins,使用以下IP,預設為8080
	
		 http://127.0.0.1:8080 

	> 預設為 8080 如要變更到 .jenkins/xxxxxxxxxxxx 
2.	進入設定畫面 , 這裡需要剛剛取得的預設密碼
![Alt text](./jenkins_Image/安裝預設密碼.png   "Optional title") 
3.	安裝套件
![Alt text](./jenkins_Image/安裝開始.png   "Optional title") 
4.	安裝成功後需要建立一組帳號
![Alt text](./jenkins_Image/建立帳號.png   "Optional title")  
5. 登入成功畫面
![Alt text](./jenkins_Image/建立新工作.png   "Optional title")
6. 擴充套件畫面
![Alt text](./jenkins_Image/模組安裝.png   "Optional title")  

 * jenkins 的強大之處就是有很多套件,並且也方便擴充
  
  * 資源管理 
  	 * Multiple SCMs plugin	 
  * CocoaPods 相關 
  	 * Integration
  * 開源整合 
  	 * gitlab , github ...
  * 測試報告
    	* JUNIT ..
   
* 有興趣了解請來這裡 
	* [https://plugins.jenkins.io/](https://plugins.jenkins.io/][https://plugins.jenkins.io/)

 
<a name="管理"></a>
## 管理

升級套件
	
	brew jenkins upgrade

安裝目錄

    /Users/hongten/.jenkins
 
刪除暫存

    /Users/hongten/.jenkins/workspace

<a name="測試流程"></a>
## 撰寫專案測試

### 所需PlugIn
* Xcode integration

### 開啟新專案

1. 點選建立新工作
![Alt text](./jenkins_Image/建立新工作.png   "Optional title")
2. 選取FreeStyle軟體專案
![Alt text](./jenkins_Image/開新專案.png   "Optional title") 
3. 輸入程式碼來源
  * a
![Alt text](./jenkins_Image/選取來源.png   "Optional title") 

![Alt text](./jenkins_Image/編譯成功.png   "Optional title") 

	
* Configure XCode Plugin.
 
1. 這裡必需設定**Configuration**為**Dubug**
![Alt text](./jenkins_Image/單元測試設定1.png   "Optional title") 

2. Code signing & OS X keychain options 裡面填入 **Team ID**
![Alt text](./jenkins_Image/單元測試設定2.png   "Optional title") 
3. Advanced Xcode build options 這裡需做些設定
	* Xcode Schema File => 這裡為專案的schema為Hello
	* Custom xcodebuild arguments => 這裡需加入的參數
	> test -destination 'platform=iOS Simulator,name=iPhone 6,OS=11.0'
	* Xcode Project Directory => 專案目錄 ./Hello/Hello
	  
	![Alt text](./jenkins_Image/單元測試設定3.png   "Optional title") 

### 啟動測試 
 * 然後就可以跑起來測試了

 
### 使用 fastlane scan 來測試 (alterntive)

1. 先安裝 xcode 工具組 

		xcode-select --install
2. 安裝 fastlane
	
		brew cask install fastlane
3. 設定fastlane環境變數
	
	fastlane requires some environment variables set up to run correctly. In particular, having your locale not set to a UTF-8 locale will cause issues with building and uploading your build. In your shell profile add the following lines:
	
		export LC_ALL=en_US.UTF-8
		export LANG=en_US.UTF-8
	
	You can find your shell profile at ~/.bashrc, ~/.bash_profile or ~/.zshrc depending on your system.

4. 專案中新增一段script,並按下儲存

		export PATH="$HOME/.fastlane/bin:$PATH"
		
		cd Hello/Hello
		
		fastlane scan -s Hello

	![Alt text](./jenkins_Image/設定fastlaneScan.png   "Optional title") 
	
	
	
4. 測試成功	
![Alt text](./jenkins_Image/fastlane測試成功.png  "Optional title") 
	

## 測試報告產出與紀錄	
[ocunit2junit 與 jenkins 配合](./ocunit2junit)
### 比較以上
由上面比較結果 fastlane 大幅減少了代碼的撰寫與專案設定，優點優於傳統自己keyIn的方法與使用jenkins xcode plugin的方法。

<a name="mitake的CI,CD流程"></a>
## mitake的CI,CD流程
 
* mitake Jenkins工作流程,主要是透過shell script來完成編譯與發版,可簡化設定流程.

![Alt text](./jenkins_Image/流程.png   "Optional title")  

  * 參數化所有流程

  		/bin/sh -xe jenkins.sh MTK MTK InHouse "2V2W2494TP" "IMTK DEV IN HOUSE" "" "" "" "" "IMTK Today Widget DEV IN HOUSE"
  		
<a name="Cluster"></a>
## Cluster

* 這裡可以透過master分配工作給slave來達到cluster,以下是建立流程.
    
1. 設定SSH的存取的資格
	* 先登入master
	* 然後下以下指令
	指令如下:
   					
   				
   				ssh-copy-id -i ~/.ssh/id_rsa.pub yourUserName@yourIp

	 ![Alt text](./jenkins_Image/設定SSH給奴隸.png   "Optional title") 
2. 在master jenkins 上選擇 Manage Jenkins -> Manage Nodes -> New Node
 ![Alt text](./jenkins_Image/slaveS.png   "Optional title") 
 ![Alt text](./jenkins_Image/slave0.png   "Optional title") 
 ![Alt text](./jenkins_Image/slave1.png   "Optional title")  
   * HOST: 主機位址
   * Usage: 最好選下Label才能使用,避免衝突
   * Launch Method: 選 SSH
  
3. 連線結果,成功會看到以下狀態
  
  ![Alt text](./jenkins_Image/Log.png   "Optional title") 

4. 在Node profile設定Provising Profile存取路徑
	> /Users/{youruser}/Library/MobileDevice/Provising\ Profile

	![Alt text](./jenkins_Image/設定NodePF路徑.png   "Optional title") 
 
5. 建議slave手動建立一次gitlab ssh(cocoaPod需要)


### 手動管理 Provision Profile and Keychanin

手動Copy keyChain and ProvisionProfile

1. keychain
   
   a. 這裡先從master複製一份到slave
   > $HOME/Library/Keychains/login-keychain.db
   
   b. 上傳到slave的路徑
   > 預設 "$HOME/LoaclKeychain/mitake.keychain"
   
   c. 然後手動加到keychains程式裡面
   
   d. 再透過以下script來判斷是否需要解鎖slave的keychain

	    #預設的slave keychain存放路徑
		KEYCHAIN_PATH=$HOME/LoaclKeychain/mitake.keychain
		
		echo ${KEYCHAIN_PATH}
		if [ -f ${KEYCHAIN_PATH} ]
		then
		    echo "keychain pw :  $KEY_CHAIN_PASSWORD"
		    security unlock-keychain -p $KEY_CHAIN_PASSWORD ${keychain} &>/dev/null
		
		    if [ $? -ne 0 ];then
		           echo "Cannot open keychain ${keychain}"
		           exit 1
		    fi
		
		    echo "unlock OK"
		fi
 
2. Provisioning Profiles
    
    a. 從master電腦複製一份到slave電腦
 > 預設存放路徑 $HOME/Library/MobileDevice/Provisioning\ Profiles/
 
     	scp $HOME/Library/MobileDevice/Provisioning\ Profiles/* mitake@10.1.4.154:/Users/mitake/Library/MobileDevice/Provisioning\ Profiles/   

### 自動管理 ProfisionProfile and Keychanin

 研擬計畫master更新一次,全部slave更新

 理論上可以使用plugin來完成但是目前需要改動每個任務的configure,所以暫延.


### 如何啟用Slave

1. 先點選專案然後選擇 Configure
2. 專案底下強制指定使用slave編譯
![Alt text](./jenkins_Image/startupSlave1.png   "Optional title")
3. 更改jenkins分支到Cluster(目前還沒合回去master)
![Alt text](./jenkins_Image/startupSlave1.png   "Optional title")
				

<a name="Webhook"></a>
## Webhook

* **Webhook 是讓一個網站能訂閱另一個網站的方法**
  		

### 為何需要Webhook ?
* 當有版本控制網站一有Push或是Merge事件觸發,可透過webhook來啟動jenkins上的任務,而達到自動化的功用
	* 通常jenkins一被叫醒就會做下面的事 
		 1. 跑起單元測試來判斷此版本是否有問題
			* 可以一併統計覆蓋率
		 2. 自動封裝測試版本
		 	* 可以馬上包出一個測試版供給測試人員使用

### 使用GitLab綁定jenkins
* gitlab 當有push or merge可以透過webhook來啟動jenkins任務,而jenkins可以達成的目標就是 :
 1. unit test or integration test (測試)
 2. build test (看是否可以編譯成功)
 3. automatic deployment 自動包板

### Webhook設定流程
### jenkins上設定

1. 先到jenkins裡面開啟專案設定

* 開啟專案 -> 設定專案 -> Tigger 事件點選 build when a change is pushed to GitLab  ...

![](./jenkins_Image/jenkins_setup.png)

* 這裏有兩點事要做
 1. 記下URL "http://10.1.4.139:8888/xxxx//xx"等下gitlab設定要用
 2. 產生Secret token,產生一組Secret token等下gitlab設定要用


### 設定GitLab

![](./jenkins_Image/gitlab_setup.png)

* 接下來去你要設定的專案 (注意你要是專案owner才有設定選項)
* 開啟 Setting -> integration
* 填入剛剛的 URL
* 填入剛剛的 Secret token
* 然後就可以測試了

### 測試
* 當每次push的時候就可以呼叫webhook,我們只需要確認是否有正確執行
* 開啟 Setting -> integration 點選webhooks 底下你的webhook 點編輯

![](./jenkins_Image/gitlab_test.png)

* 然後你就可以看你的最近 Deliveries 是否有推成功

![](./jenkins_Image/gitlab_test1.png)


## Fastlane 
* astlane action /// 查詢build-in元件


fastlane cert 

		fastlane sigh --team_id 4DQLE3NABV --app_identifier com.a-mtk.testapp --username amtkApp@gmail.com
		
* 增加一個internet-password
		
		security add-internet-password -a "amtkApp@gmail.com" -w "xxxxxxxxx" -s "deliver.amtkApp@gmail.com"


<a name="參考"></a>
##參考

[升級到 9.0 打包失敗](http://blog.csdn.net/kongdeqin/article/details/78050599)

[cocoaPod 問題一些設定問題](http://www.jianshu.com/p/969dcb9907cf)


[安裝 jenlins](http://flummox-engineering.blogspot.com/2016/01/installing-jenkins-os-x-homebrew.html)

[jenkins unit test教學](https://medium.com/@jerrywang0420/unit-test-ui-test-jenkins-c-i-教學-ios-800cab673b6d)

[jenkins slave setup](https://blog.samsao.co/how-to-setup-a-jenkins-slave-running-mac-os-x-for-ios-projects-part-1-2937502ce90b)


[jenkins ssk error](https://stackoverflow.com/questions/41734737/why-jenkins-says-server-rejected-the-1-private-keys-while-launching-the-agen)

[ Gitlab WEBHOOK on jenkins
] (https://github.com/elvanja/jenkins-gitlab-hook-plugin#parameterized-projects
)